# AVType

AVType is a tool to identify the type of malicious files. AVType attempts to group known malicious files into types. For each malicious file it uses multiple AV labels to derive their behavior type (e.g., fakeAV, ransomware, dropper, etc.).

AVType receives as input the anti virus (AV) labels for a given file and produces the type of malware based on the AV labels. 

For more details about the purpose of this tool and how it works, please refer to our DSN 2017 paper titled: "Exploring the Long Tail of (Malicious) Software Downloads" (Babak Rahbarinia, Marco Balduzzi, Roberto Perdisci).

**Disclaimer**: Due to high amount of noise and inconsistency in anti virus labels, this tool works based on *best effort* approach and is not guaranteed to produce expected results. We encourage manual analysis of the results.

## Type Extraction

AVType expects a JSON input file. Each line of the input should be a dictionary with 2 keys: i) "sha1" ("md5" and "sha256" are also accepted) and ii) "av_labels". The "av_labels" is a dictionary of AV's and their AV labels. The following is an example of one line of input:

```
{"sha1": "3f2fee39787ca4c69882485390ffa89fef84b397", "av_labels": {"Kaspersky": "HEUR:Trojan-Downloader.Win32.Generic"}}
```

**Note**: We plan to add support for Virus Total files in near future.

AVType analyzes the AV labels and assigns an AVType to the file. In the case of example above, the type would be determined as "dropper".

Currently, AVType understands the labels of 5 AVs: Microsoft, Symantec, McAfee, Kaspersky, and Trend Micro. The more obvious labels of other AVs might also be understood by AVType.

AVType could categorize the malware types into: trojan, banker, bot, adware, pup, ransomware, spyware, fakeav, worm, dropper, and generic.

**How To Run?**

AVType is written in Python (Python 2.7). It accept 3 input arguments as follows:

```
usage: avtype.py [-h] -i INPUT [-m MODE] [-v]

AVType

optional arguments:
  -h, --help  show this help message and exit
  -i INPUT    input AV labels file
  -m MODE     3 modes: unanimous, majority, aggressive
  -v          verbose output for debuging
```

The MODE flag accepts 3 possible inputs: i) unanimous: only assigns a type if *all* AV labels agree with each other, ii) majority: assigns a type if *majority* (>50%) of AV labels agree with each other, and iii) aggressive: makes best effort to assign a type, for example, by assigning the most *specific* type among the types extracted from AV labels. The aggressive mode might produce unexpected results. For example, if a file has two AV labels and one AV's type is spyware and one AV's type is adware, then AVType cannot decide between the two and will produce trojan as output since it covers both spyware and adware.

We recommend to provide the verbose flag at all times. It provides more information about why AVType decided to assign a specific type to a given file based on the types extracted from each individual AV label.

## LICENSING

The code is released under GNU GENERAL PUBLIC license. Please refer to the COPYING file for details.
